﻿using System;
using System.Linq;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;

namespace ExamplesEF
{
    class Program
    {
        static void Main()
        {
            // Создаем экземпляр класса контекста 
            SampleContext context = new SampleContext();

            context.Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));

            // Используем LINQ-запрос для извлечения первого заказчика
            var name = context.Customers
                              .Select(c => c.FirstName)
                              .FirstOrDefault();

            Console.WriteLine(name);
        }        
    }
}
