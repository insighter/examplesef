namespace ExamplesEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderLines",
                c => new
                    {
                        OrderLinesId = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                        Order_OrderId = c.Int(),
                    })
                .PrimaryKey(t => t.OrderLinesId)
                .ForeignKey("dbo.Orders", t => t.Order_OrderId)
                .Index(t => t.Order_OrderId);
            
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        CustomerId = c.Int(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        LastLoginDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .Index(t => t.CustomerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Profiles", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.OrderLines", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.Profiles", new[] { "CustomerId" });
            DropIndex("dbo.OrderLines", new[] { "Order_OrderId" });
            DropTable("dbo.Profiles");
            DropTable("dbo.OrderLines");
        }
    }
}
