﻿using System.Data.Entity;

namespace ExamplesEF
{
    public class SampleContext : DbContext
    {
        public SampleContext() : base("MyShop")
        {
            //// Указывает EF, что если модель изменилась,
            //// нужно воссоздать базу данных с новой структурой
            //Database.SetInitializer(
            //    new DropCreateDatabaseIfModelChanges<SampleContext>());
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}