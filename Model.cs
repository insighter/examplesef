﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExamplesEF
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }

        [Column(TypeName = "Image")]
        public byte[] Photo { get; set; }

        // Ссылка на заказы
        public List<Order> Orders { get; set; }

        public Profile Profile { get; set; }
    }

    public class Order
    {
        public int OrderId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public DateTime PurchaseDate { get; set; }

        // Ссылка на покупателя
        public Customer Customer { get; set; }

        public List<OrderLines> Lines { get; set; }
    }

    public class Profile
    {
        [Key]
        [ForeignKey("CustomerOf")]
        public int CustomerId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime LastLoginDate { get; set; }

        public Customer CustomerOf { get; set; }
    }

    public class OrderLines
    {
        public int OrderLinesId { get; set; }
        public string Address { get; set; }
    }
}